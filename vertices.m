function [ v ] = vertices( A, b )
	[m, n]=size(A);
	
	comb=nchoosek(1:m, n);
	n_comb=size(comb, 1);
	v=[];
	
	for i=1:n_comb
		A_part=A(comb(i, :), :);
		b_part=b(comb(i, :), :);
		if findRank(A_part)==n
			v=[v; (A_part\b_part)'];
		end
	end
	
	%checking for feasible vertices
	v=v(sum(bsxfun(@le, A*v', b))==m, :);
end
