function [ r, ref ] = findRank( A )
	[m, n]=size(A);

	%converting matrix to row echelon form
	for i=1:min(m,n)
		%finding first non-zero entry in the column
		col=A(i:m, i);
		nz=find(col);
		if size(nz, 1)==0
			continue
		end
		nz_i=i+nz(1)-1;
		
		%swap rows if required
		if i~=nz_i
			A([i, nz_i], :)=A([nz_i, i], :);
		end
		
		%normalize pivoted row
		A(i, :)=A(i, :)/A(i,i);
		
		%subtract pivot row from lower rows
		for j=i+1:m
			A(j, :)=A(j, :)-A(i, :)*A(j, i);
		end
	end
	
	ref=A;
	r=nnz(diag(ref));
end
