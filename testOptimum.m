[op_v, op_c, path]=findOptimum(A,b,c,x);
plotpolytope(A, b);
hold on;
plot(x(1), x(2), 'go');
plot(op_v(1), op_v(2), 'bo');

for i=1:size(path, 1)-1
	plot(path([i, i+1], 1), path([i, i+1], 2), 'r');
end

hold off;
