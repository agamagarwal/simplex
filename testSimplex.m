try
	[op_v, op_c, path]=solve(A,b,c);
catch exception
	fprintf('Exiting\n');
	return
end
fprintf(['Optimal vertex = ' num2str(op_v') '\n'])
fprintf(['Optimal cost = ' num2str(op_c) '\n'])

if size(A, 2)==2
plotpolytope(A, b);
hold on;

for i=1:size(path, 1)-1
	plot(path([i, i+1], 1), path([i, i+1], 2), 'r');
end

plot(path(:, 1), path(:, 2), 'cs');
plot(op_v(1), op_v(2), 'ks', 'MarkerFaceColor', 'black');

text(op_v(1)+0.1, op_v(2)+0.1, ['Optimum Cost = ' num2str(op_c)]);

hold off;
end