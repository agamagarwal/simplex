function [ optimal_vertex, optimal_cost, path ] = findOptimum( A, b, c, feasible_point, ignore_unbounded )
	
	if ~exist('ignore_unbounded', 'var') || isempty(ignore_unbounded)
		ignore_unbounded=false;
	end
	
	[m, n]=size(A);
	
	%check if feasible point
	feasible_count=nnz(A*feasible_point<=b);
	if feasible_count<m
		throw(MException('Simplex:notFeasiblePoint', 'Given point is not a feasible point'));
	end
	
	path=feasible_point';
	
	%find number of tight rows
	num_tight=nnz(floatEqual(A*feasible_point, b));
	
	%if no rows are tight, move to one
	if num_tight==0
		%finding closest row
		distances=(A*feasible_point-b)./diag(A*A');
		
		[~, idx]=min(abs(distances));
		
		%move to row A(idx, :)
		feasible_point=feasible_point-A(idx, :)'*distances(idx);
		path=[path; feasible_point'];
	end
	
	%starting vertex
	[vertex, tight_idx, temp_path]=findOptimalVertex(A, b, feasible_point);
	path=[path; temp_path];
	
	
	%find tight rows
	A_tight=A(tight_idx, :);
	b_tight=b(tight_idx);
	A_loose=A(~tight_idx, :);
	b_loose=b(~tight_idx);
	
	%jump from vertex to vertex
	while true
		cur_cost=c*vertex;
		
		%check if not degenerate
		if size(A_tight, 1)>n
			throw(MException('Simplex:degenerate', 'The given system is degenerate.'));
		end
		
		%direction matrix is the negative of the inverse of the tight rows
		directions=-inv(A_tight);
		
		%check if already optimal
		[max_cost, idx]=max(c*(1*directions+repmat(vertex, 1, n)));
		
		%if reached max
		if max_cost<=cur_cost
			break
		else
			direction=directions(:, idx);
			
			distance=zeros(size(A_loose, 1), 1);
			
			for i=1:size(A_loose, 1)
				cur_prod=A_loose(i, :)*direction;
				if cur_prod<=0
					distance(i)=nan;
				else
					distance(i)=(b_loose(i)-A_loose(i, :)*vertex)./cur_prod;
				end
			end
			
			%if unbounded, try another direction
			if nnz(isnan(distance))==size(A_loose, 1)
				if ignore_unbounded
					break;
				else
					throw(MException('Simplex:unbounded', 'System is unbounded'));
				end
			end
			
			[~, min_idx]=min(abs(distance));
			min_distance=distance(min_idx);
			vertex=vertex+min_distance*direction;
			path=[path; vertex(:)'];
			
			%Update loose and tight rows
			loose_idx=find(~floatEqual(A_tight*vertex, b_tight));

			temp_A=A_tight(loose_idx, :);
			temp_b=b_tight(loose_idx);
			A_tight(loose_idx, :)=A_loose(min_idx, :);
			b_tight(loose_idx)=b_loose(min_idx);
			A_loose(min_idx, :)=temp_A;
			b_loose(min_idx)=temp_b;
		end
	end
	
	optimal_vertex=vertex;
	optimal_cost=c*vertex;
end
