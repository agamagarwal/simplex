function [ optimal_vertex, optimal_cost, path ] = solveSimplex( A, b, c )
	
	%find initial feasible point
	[m, n]=size(A);
	b_min=min(b);
	A_new=[A, ones(m, 1); zeros(1, n), -1];
	b_new=[b; -b_min];
	c_new=[zeros(1, n), 1];
	feasible_new=[zeros(n, 1); b_min];
	[optimal_vertex_new, optimal_cost_new]=findOptimum(A_new, b_new, c_new, feasible_new, true);
	
	%if infeasible
	if optimal_cost_new<0
		throw(MException('Simplex:infeasible', 'System is infeasible'));
	end
	
	feasible=optimal_vertex_new(1:end-1);
	
	[optimal_vertex, optimal_cost, path]=findOptimum(A, b, c, feasible);
	
end

