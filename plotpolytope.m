function plotpolytope( A,b )
	
	if size(A, 2)>2
		error('failed')
	end
	
	m=size(A, 1);
	figure; hold on;
	
	x=[-100, 100];
	
	for i=1:m
		if A(i,2)~=0
			slope=-A(i,1)/A(i,2);
			intercept=b(i)/A(i,2);
			plot(x, slope*x+intercept);
		else
			plot(ones(1,2)*(b(i)/A(i,1)), [-100, 100]);
		end
	end
	
	v=vertices(A,b);
	plot(v(:,1), v(:,2), 'r*');
	
	hold off;
end
