function [ res ] = floatEqual( a,b )
	res=abs(a-b)<1e-8;
end
