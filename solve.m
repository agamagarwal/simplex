function [ optimal_vertex, optimal_cost, path ] = solve( A, b, c )
	
	[m, n]=size(A);
	
	if findRank(A)<n
		rank_deficient=true;
		A_old=A;
		b_old=b;
		c_old=c;
		
		A=zeros(m+2*n, 2*n);
		c=zeros(1, 2*n);
		A(1:m, 1:2:end)=A_old;
		A(1:m, 2:2:end)=-A_old;
		c(1:2:end)=c_old;
		c(2:2:end)=-c_old;
		A(m+1:end, :)=-eye(2*n, 2*n);
		b=[b_old; zeros(2*n, 1)];
	else
		rank_deficient=false;
	end
	
	try
		[optimal_vertex, optimal_cost, path]=solveSimplex(A, b, c);
	catch exception
		if strcmp(exception.identifier, 'Simplex:degenerate')
			
			epsilon=0.1;
			while true
				disturbance=epsilon.^(1:size(A, 1))';
				b_new=b+disturbance;
				try
					[optimal_vertex, ~, path]=solveSimplex(A, b_new, c);
				catch exception2
					if strcmp(exception2.identifier, 'Simplex:degenerate')
						epsilon=epsilon/2;
						if epsilon<1e-100
							fprintf('Unable to resolve degeneracy.\n');
							return;
						end
						continue;
					else
						fprintf('%s\n', exception2.message);
						rethrow(exception2);
					end
				end
				
				%solution has been found
				break;
			end
			
			%restore to old system
			tight_idx=bsxfun(@floatEqual, A*optimal_vertex, b_new);
			optimal_vertex=A(tight_idx, :)\b(tight_idx);
			path=[path; optimal_vertex'];
			optimal_cost=c*optimal_vertex;
			
		else
			fprintf('%s\n', exception.message);
			rethrow(exception);
		end
	end
	
	if rank_deficient
		optimal_vertex=optimal_vertex(1:2:end)-optimal_vertex(2:2:end);
		path=path(:, 1:2:end)-path(:, 2:2:end);
	end
	
end

