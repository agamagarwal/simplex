function [ optimal_vertex, tight_idx, path ] = findOptimalVertex( A, b, optimal_point )
	
	[~, n]=size(A);
	
	%find number of tight rows
	tight_idx=floatEqual(A*optimal_point, b);
	num_tight=nnz(tight_idx);
	
	path=[];
	
	if num_tight==0
		throw(MException('Simplex:notBoundary', 'Not a boundary point'));
	else
		optimal_vertex=optimal_point;
		while num_tight<n
			A_tight=A(tight_idx, :);
			A_loose=A(~tight_idx, :);
			b_loose=b(~tight_idx);

			%move in direction of null space
			directions=null(A_tight, 'r');
			
			for i=1:size(directions, 2)
				distance=zeros(size(A_loose, 1), 1);

				for j=1:size(A_loose, 1)
					cur_prod=A_loose(j, :)*directions(:, i);
					if cur_prod==0
						distance(j)=nan;
					else
						distance(j)=(b_loose(j)-A_loose(j, :)*optimal_vertex)./cur_prod;
					end
				end
				
				%if unbounded, try another direction
				if nnz(isnan(distance))==size(A_loose, 1)
					%if last direction
					if i==size(directions, 2)
						throw(MException('Simplex:unbounded', 'System is unbounded'));
					end
					continue;
				end

				[~, min_idx]=min(abs(distance));
				min_distance=distance(min_idx);
				optimal_vertex=optimal_vertex+min_distance*directions(:, i);
				path=[path; optimal_vertex(:)'];
				break;
			end
			%find new tight and loose rows space
			tight_idx=floatEqual(A*optimal_vertex, b);
			num_tight=nnz(tight_idx);
		end
	end
end
